import { Component, OnDestroy, OnInit } from '@angular/core';
import { interval, Observable, Subscription } from 'rxjs';
import { map, filter } from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {
  private firstObsSubscription: Subscription;

  constructor() {
  }

  ngOnInit() {
    const customIntervalObservable = new Observable(observer => {
      let count = 0;
      setInterval(() => {
        observer.next(count);
        if (count === 2) {
          observer.complete();
        }
        if (count > 3) {
          observer.error(new Error('Count > 3'));
        }
        count++;
      }, 1000);
    });

    this.firstObsSubscription = customIntervalObservable
      .pipe(
        filter(data => {
          return 0 !== data;
      }),
        map(data => {
          return `Round: ${data}`;
      }))
      .subscribe(count => {
        console.log(count);

      }, error => {
        console.log(error);

      }, () => {
        console.log('Completed!');

      });
    // this.firstObsSubscription = interval(1000).subscribe((count) => {
    //   console.log(count);
    // });
    // setInterval(() => {
    //   console.log(5);
    // }, 1000);
  }

  ngOnDestroy(): void {
    this.firstObsSubscription.unsubscribe();
  }

}
