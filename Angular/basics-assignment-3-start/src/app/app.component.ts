import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  isShow = true
  counterClick = []

  toggleText(){
    this.isShow = !this.isShow
    this.counterClick.push(new Date())
  }
}
