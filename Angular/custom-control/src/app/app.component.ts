import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements OnInit {
  title = false;
  form = this.fb.group({
    name: ['', Validators.compose([Validators.required])],
    custom: [null, Validators.compose([Validators.required, Validators.min(10)])],
  })

  constructor(private fb: FormBuilder) {

  }
  
  ngOnInit() {
    this.form.valueChanges.subscribe(values => {
      console.log(this.form.controls);
      console.log(values, this.form.valid);
    })
  }

  onSubmit() {
    console.log('form touched', this.form.touched);
    this.form.markAllAsTouched();
  }
  onValid() {
    this.form.updateValueAndValidity();
  }
  onDis() {
    if (this.form.disabled) {
      this.form.enable();
    } else {
      this.form.disable();
    }
  }
  onReset() {
    this.form.reset();
  }
}
