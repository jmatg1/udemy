import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectorJustControlComponent } from './selector-just-control.component';

describe('SelectorJustControlComponent', () => {
  let component: SelectorJustControlComponent;
  let fixture: ComponentFixture<SelectorJustControlComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SelectorJustControlComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SelectorJustControlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
