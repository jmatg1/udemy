import { Component, Input, OnInit } from '@angular/core';
import { AbstractControl, FormControl } from '@angular/forms';

@Component({
  selector: 'app-selector-just-control',
  templateUrl: './selector-just-control.component.html',
  styleUrls: ['./selector-just-control.component.sass']
})
export class SelectorJustControlComponent implements OnInit {

  @Input() control!: FormControl;

  constructor() { }

  ngOnInit(): void {
    this.control.statusChanges.subscribe(res => {
      console.log(res);
    })
  }

}
