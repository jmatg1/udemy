import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectorProviderComponent } from './selector-provider.component';

describe('SelectorProviderComponent', () => {
  let component: SelectorProviderComponent;
  let fixture: ComponentFixture<SelectorProviderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SelectorProviderComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SelectorProviderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
