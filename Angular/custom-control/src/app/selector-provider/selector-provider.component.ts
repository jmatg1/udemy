import { Component, Injector, OnInit } from '@angular/core';
import {
  AbstractControl,
  ControlValueAccessor,
  FormControl, NG_VALIDATORS, NG_VALUE_ACCESSOR,
  NgControl,
  ValidationErrors,
  Validator
} from '@angular/forms';

@Component({
  selector: 'app-selector-provider',
  templateUrl: './selector-provider.component.html',
  styleUrls: ['./selector-provider.component.sass'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: SelectorProviderComponent,
    },
    {
      provide: NG_VALIDATORS,
      multi: true,
      useExisting: SelectorProviderComponent,
    }
  ]
})
export class SelectorProviderComponent implements OnInit, ControlValueAccessor, Validator {
  control = new FormControl('')

  constructor(private inj: Injector) {
  }

  ngOnInit(): void {
    console.log(this);
    this.control.valueChanges.subscribe(val => {
      this.onChange(val);
    })
  }

  ngDoCheck() {
    console.log();
  }

  onChange = (value: string | null) => null;
  onTouched = () => null;

  registerOnChange(fn: (value: string | null) => null) {
    this.onChange = fn;
  }

  registerOnTouched(fn: any) {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean) {
    if (isDisabled) {
      this.control.disable();
    } else {
      this.control.enable();
    }
  }

  writeValue(obj: any) {
    console.log(obj);
    this.control.setValue(obj);
  }

  validate(control: AbstractControl): ValidationErrors | null {
    return null
  }


}
