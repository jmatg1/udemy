import { Component, DoCheck, OnInit, Optional, Self } from '@angular/core';
import {
  AbstractControl,
  ControlValueAccessor,
  FormControl,
  NG_VALIDATORS,
  NG_VALUE_ACCESSOR, NgControl,
  ValidationErrors,
  Validator
} from '@angular/forms';

@Component({
  selector: 'app-selector',
  templateUrl: './selector.component.html',
  styleUrls: ['./selector.component.sass'],
})
export class SelectorComponent implements OnInit, DoCheck, ControlValueAccessor, Validator {
  control = new FormControl('')

  constructor(public ngControl: NgControl) {
    if (Boolean(ngControl)) {
      ngControl.valueAccessor = this;
    }
  }

  ngOnInit(): void {
    if (this.ngControl.control) {
      this.control.setValidators(this.ngControl.control.validator);
    }
    this.control.valueChanges.subscribe(val => {
      this.onChange(val);
    })
  }

  ngDoCheck() {
    this.checkTouchFromParentControl();
  }

  checkTouchFromParentControl() {
    if (this.ngControl.touched && this.control.touched) {
      console.log('this.ngControl.touched');
      return
    }
    if (this.ngControl.touched && this.control.untouched) {
      console.log('markAsTouched');
      this.control.markAsTouched()
    }
    if (this.ngControl.untouched && this.control.touched) {
      console.log('markAsUntouched');
      this.control.markAsUntouched()
    }
  }

  onChange = (value: string | null) => null;
  onTouched = () => null;

  registerOnChange(fn: (value: string | null) => null) {
    this.onChange = fn;
  }

  registerOnTouched(fn: any) {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean) {
    if (isDisabled) {
      this.control.disable();
    } else {
      this.control.enable();
    }
  }

  writeValue(obj: any) {
    if (obj === null) {
      this.checkTouchFromParentControl()
    }
    this.control.setValue(obj);
  }

  validate(control: AbstractControl): ValidationErrors | null {
    return null
  }

}
