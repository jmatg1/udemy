import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  form: FormGroup;
  projectStatusOptions = ['Stable', 'Critical', 'Finished'];


  ngOnInit(): void {
    this.form = new FormGroup({
      projectName: new FormControl('', Validators.required, this.forbiddenNames.bind(this) ),
      email: new FormControl('', [Validators.required, Validators.email]),
      projectStatus: new FormControl(this.projectStatusOptions[2])
    });
  }

  onSubmit () {
    console.log(this.form.value);
  }

  forbiddenNames (control: FormControl) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        if (control.value === 'test') {
          resolve({ nameIsForbidden: true});
        }
        return null
      }, 1500);
    });
  }


}
