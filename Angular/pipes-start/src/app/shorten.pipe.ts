import { Pipe, PipeTransform } from '@angular/core';

/**
 * Вставляется в html например
 * {{ name | Shorten }}
 */
@Pipe({
  name: 'shorten'
})
export class  ShortenPipe implements  PipeTransform {
  transform(value: string, limit: number, ellipsis: boolean): any {
    if (value.length > limit) {
      let newValue = value.substr(0, limit);
      if (ellipsis) {
        newValue += ' ...';
      }
      return newValue;
    }
    return value;
  }
}
