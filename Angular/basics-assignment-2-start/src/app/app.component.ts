import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  username: string = ''
  disabledClearButton: boolean = false



  onClearUserName() {
    if (this.username.length > 0) {
     return this.username = ''
    }
    console.log('click')
  }

  statusClearButton() {
    return this.username.length === 0
  }
}
