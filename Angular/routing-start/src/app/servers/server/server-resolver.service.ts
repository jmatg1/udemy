import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { ServersService } from '../servers.service';
import { Injectable } from '@angular/core';

interface Server {
  id: number;
  name: string;
  status: string;
}

@Injectable()
/**
 * Здесь можно выполнять запросы к серверу.
 * Получается компонент не будет загружен
 * пока не вернет что-нибудь этот resolver
 */
export class ServerResolver implements Resolve<Server> {
  constructor(private serversService: ServersService) {
  }
  resolve(route: ActivatedRouteSnapshot,
          state: RouterStateSnapshot): Observable<Server> | Promise<Server> | Server {
    // return new Promise((resolve, reject) => {
    //   setTimeout(() => {
    //     resolve(this.serversService.getServer(+route.params['id']));
    //   }, 500);
    // });
    return this.serversService.getServer(+route.params['id'])
  }
}
