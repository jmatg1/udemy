import { Component, OnInit } from '@angular/core';

import { ServersService } from '../servers.service';
import {ActivatedRoute, Params, Router} from '@angular/router';
import { CanComponentDeactivate } from "./can-deactivate-guard.service";
import { Observable } from "rxjs";

@Component({
  selector: 'app-edit-server',
  templateUrl: './edit-server.component.html',
  styleUrls: ['./edit-server.component.css']
})
export class EditServerComponent implements OnInit, CanComponentDeactivate {
  server: { id: number, name: string, status: string };
  serverName = '';
  serverStatus = '';
  allowEdit = false;
  changesSaved = false

  constructor (private serversService: ServersService,
               private route: ActivatedRoute,
               private router: Router) {
  }

  ngOnInit () {
    const id = this.route.snapshot.params.id;
    this.route.queryParams
      .subscribe((params: Params) => {
        this.allowEdit = params['allowEdit'] === '1';
      });
    // console.log(this.router.snapshot.params)
    // console.log(this.router.snapshot.queryParams)
    // console.log(this.router.snapshot.fragment)

    this.server = this.serversService.getServer(Number(id));
    this.serverName = this.server.name;
    this.serverStatus = this.server.status;
  }

  onUpdateServer () {
    this.serversService.updateServer(this.server.id, {
      name: this.serverName,
      status: this.serverStatus
    });
    this.changesSaved = true
    this.router.navigate(['../'], { relativeTo: this.route, queryParamsHandling: 'merge' })
  }

  canDeactivate() {
    console.log('in', !this.allowEdit)

   if (!this.allowEdit) {
     return true
   }
   if ((this.serverName !== this.server.name || this.serverStatus !== this.server.status) && !this.changesSaved) {
     return confirm('Do you want to discard the changes?')
   } else {
     return true
   }
  }
}
