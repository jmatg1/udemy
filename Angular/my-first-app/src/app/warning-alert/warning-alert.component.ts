import { Component } from '@angular/core'

@Component({
  selector: 'app-warning-success',
  template: `
    <div class="super-text">Всё супер{{text}}</div>
    <button (click)="onChangeText()">Yes</button>
  `,
  styles: [`
    .super-text {
      color: #5C9210;
    }
  `]
})

export class WarningAlertComponent {
  text = ''

  onChangeText () {
    this.text = '- LIE'
  }
}
