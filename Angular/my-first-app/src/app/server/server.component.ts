import { Component } from "@angular/core"

@Component({
  selector: 'app-server',
  templateUrl: './server.component.html'
})
export class ServerComponent {
  serverId: number = 25
  serverStatus: string = 'offline'

  constructor() {
    this.serverStatus = Math.random() > 0.5 ? 'online' : 'offline'
  }

  getServerStatus () {
    return this.serverStatus
  }

  getColor () {
    console.log('render')

    return  Math.random() > 0.5 ? 'red' : 'green'
  }
}
