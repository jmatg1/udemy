import { Injectable } from '@angular/core';
import { HttpClient, HttpEventType, HttpHeaders, HttpParams } from '@angular/common/http';
import { Post } from './post.model';
import { catchError, map, tap } from 'rxjs/operators';
import { Subject, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PostsService {
  private apiPosts = 'https://ng-complete-guid-e5377.firebaseio.com/posts.json';
  error = new Subject<string>();
  constructor(private http: HttpClient) { }

  createAndStorePost(title: string, content: string) {
    // Send Http request
    const postData: Post = {
      title,
      content
    };

    let searchParams = new HttpParams()
    searchParams = searchParams.append('query', 'Car')
    searchParams = searchParams.append('price', '152.(*&*(%00')
    return this.http.post<{name: string}>(
      this.apiPosts,
      postData,
      {
        observe: 'response', // чтобы пришел полный ответ, тоесть полный объект с кодом ошибки со всем
        headers: new HttpHeaders({
          'Custom-Header': 'Hi Bro'
        }),
        params: searchParams
      })
      .subscribe(response => {
        console.log(response, 'post added')

      }, error => {
        this.error.next(error.message);
      });
  }

  fetchPosts() {
    return this.http
      .get<{[key: string]: Post}>(this.apiPosts) // { responseType: 'text' }
      .pipe(map((responseData) => {
        const postsArray: Array<Post> = [];

        for (const key in responseData) {

          if (responseData.hasOwnProperty) {
            postsArray.push({
              id: key,
              ...responseData[key]
            });
          }
        }
        return postsArray;
      }),
        catchError(error => {
          // to do something
          return throwError(error);
        }));
  }

  deletePosts() {
    return this.http.delete(
      this.apiPosts, {
        observe: 'events'
      })
      .pipe(tap( event => {
        console.log(event, event.type === HttpEventType.Sent)

      }));
  }
}
