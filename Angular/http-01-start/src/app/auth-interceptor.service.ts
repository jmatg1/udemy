import { HttpEvent, HttpEventType, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from "rxjs/operators";

export class AuthInterceptorService implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    console.log('Был отправлен запрос')
    const modifiedRequest = req.clone({
      headers: req.headers.append('Authorization---', 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJ')
    })
    return next.handle(modifiedRequest);
  }
}
//
// export class AuthInterceptorService implements HttpInterceptor {
//   intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
//     console.log('Был отправлен запрос')
//     const modifiedRequest = req.clone({
//       // headers: req.headers.append('Authorization', 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJ')
//     })
//     return next.handle(modifiedRequest).pipe(tap(event => {
//       console.log(event)
//       if (event.type === HttpEventType.Response) {
//         console.log(`Event resopnse:`, event.body)
//
//       }
//     }));
//   }
// }
