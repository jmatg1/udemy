import { Component, OnDestroy, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Post } from './post.model';
import { PostsService } from './posts.service';
import { Subscription } from "rxjs";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit, OnDestroy {
  loadedPosts: Post[] = [];
  isFetching = false;
  error = null;
  private errorSub: Subscription
  private apiPosts = 'https://ng-complete-guid-e5377.firebaseio.com/posts.json';

  constructor(private http: HttpClient, private postsService: PostsService) {}

  ngOnInit() {
    this.onFetchPosts();
    this.errorSub = this.postsService.error
      .subscribe(er => {
        this.error = er;
      });
  }
  ngOnDestroy(): void {
    this.errorSub.unsubscribe()
  }

  onCreatePost(postData: Post) {
    this.isFetching = true;
    this.postsService
      .createAndStorePost(postData.title, postData.content);
  }

  onFetchPosts() {
    // Send Http request
    this.isFetching = true;
    this.postsService.fetchPosts()
      .subscribe(response => {
        this.loadedPosts = response;
        this.isFetching = false;
      }, (error: Error) => {
        this.isFetching = false;
        this.error = error.message;
      });
  }

  onClearPosts() {
    // Send Http request
    this.isFetching = true;
    this.postsService.deletePosts()
      .subscribe((response) => {
        this.loadedPosts = [];
        this.isFetching = false;
      });
  }

  onHandleError() {
    this.error = null
  }
}
