import { Component, OnInit } from '@angular/core';
import { UserService } from './services/user.service';
import { CounterService } from "./services/counter.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  activeUsers: Array<string> = [];
  inactiveUsers: Array<string> = [];
  inactiveToActiveCounter: number
  activeToInactiveCounter: number

  constructor(private userService: UserService, private counterService: CounterService) {

  }

  ngOnInit(): void {
    this.activeUsers = this.userService.activeUsers;
    this.inactiveUsers = this.userService.inactiveUsers;
    this.inactiveToActiveCounter = this.counterService.inactiveToActiveCounter;
    this.activeToInactiveCounter = this.counterService.activeToInactiveCounter;
  }
}
