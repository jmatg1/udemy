import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  numbers: Array<Number> = []

  changeNumber() {
    this.numbers.push(this.numbers.length + 1)
  }
}
