import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-game-control',
  templateUrl: './game-control.component.html',
  styleUrls: ['./game-control.component.css']
})
export class GameControlComponent implements OnInit {
  interval;
  @Output() changeNumber = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  onStartGame() {
    this.interval = setInterval(()=>{
      this.changeNumber.emit()
    }, 1000)
  }

  onStopGame() {
    clearInterval(this.interval)
  }
}
