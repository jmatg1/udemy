import React, { Component } from 'react';
import Link from 'next/link'

class IndexPage extends Component {
    static getInitialProps(context) {
        console.log(context)
        const promise = new Promise((resolve, reject) => {
            setTimeout(()=>{
                resolve({appName: 'Super App'})
            }, 6000)
        })

        return promise
    }
    render() {
        return (
            <div>
                <h1>The index page of{this.props.appName}</h1>
                <Link href='/auth'>Go to Auth page</Link>
            </div>
        )
    }

};

export default IndexPage;
