import {useState} from 'react'

export const useFormInput = () => {
  const [value, setValue] = useState('')
  const [validity, setValidity] = useState(false)

  const inputChangeHandler = ev => {

    console.log(111)

    setValue(ev.target.value)
    if (ev.target.value.trim() === ''){
      setValidity(false)
    } else {
      setValidity(true)
    }
  }

  return { value: value, onChange: inputChangeHandler , validity: validity}
}

