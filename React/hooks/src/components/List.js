import React from 'react';

const List = (props) => {
  
  console.log('render list')
  
  return (
    <ul>
      {props.items.map(todo => <li key={todo.key} onClick={props.onDeleteItem.bind(null,todo.key)}>{todo.name}</li>)}
    </ul>
  );
};

export default List;
