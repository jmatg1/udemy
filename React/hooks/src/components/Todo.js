import React, {useState, useEffect, useReducer, useRef, useMemo} from 'react';
import axios from 'axios'
import List from "./List";
import {useFormInput} from '../myHooks/forms'

const Todo = (props) => {
  // const [todoName, setTodoName] = useState('')
  // const [todoList, setTodoList] = useState([])
  // const [todoState, setTodoState] = useState({
  //   userInput: '',
  //   todoList: []
  // })
  // const [submittedTodo, setSubmittedTodo] = useState(null)
  // const [validInput, setValidInput] = useState(false)
  // const todoInputRef = useRef()

  const myUseForm = useFormInput()

  const todoListReducer = (state, action) => {
    switch (action.type) {
      case 'SET':
        return action.payload
      case 'ADD':
        return state.concat(action.payload)
      case 'REMOVE':
        return state.filter((todo) => todo.key !== action.payload.id)
      default:
        return state;
    }
  }

  const [todoList, dispatch] = useReducer(todoListReducer, [])

  useEffect(() => {
    axios.get('https://jsonplaceholder.typicode.com/todos/')
      .then(res => {
        const todoData = []
        res.data.map((todo, i) => {
          if (i > 10) return
          todoData.push({
            key: todo.id,
            name: todo.title
          })
        })
        dispatch({type: 'SET', payload: todoData})

      })
      .catch(err => console.log(err))
  }, [])

  useEffect(() => {
    document.addEventListener('mousemove', handleMouseMove)
    return () => document.removeEventListener('mousemove', handleMouseMove)
  }, [])

  // useEffect(() => {
  //   if (submittedTodo) {
  //     dispatch({type: 'ADD', payload: submittedTodo})
  //   }
  // }, [submittedTodo])

  // const handleChangeInput = (ev) => {
  //   setTodoName(ev.target.value)
  // }

  const handleMouseMove = (ev) => {
    // console.log(ev.clientX);
    return false
  }

  const handleTodoAdd = () => {
    const todoName = myUseForm.value
    axios.get('https://jsonplaceholder.typicode.com/todos/1')
      .then(res => {
        setTimeout(() => {
          const todoItem = {key: Math.random(), name: todoName}
          dispatch({type: 'ADD', payload: todoItem})
        }, 3000)
      })
      .catch(err => console.log(err))
  }

  const handleTodoRemove = (todoId) => {

    dispatch({type: 'REMOVE', payload: {id: todoId}})
  }

  // const handleValidInput = ev => {
  //   if (ev.target.value.trim() === '') {
  //     setValidInput(false)
  //   } else {
  //     setValidInput(true)
  //   }
  // }
  return (
    <>
      <input type="text" value={myUseForm.value} onChange={myUseForm.onChange}
             style={{backgroundColor: myUseForm.validity ? 'white' : 'red'}}/>
      <button onClick={handleTodoAdd}>Add</button>
      {useMemo(() => (
        <List items={todoList} onDeleteItem={handleTodoRemove}/>
        ),
        [todoList])
      }
    </>
  );
};

export default Todo;
