import React, { Component } from "react";
import { Transition, TransitionGroup } from "react-transition-group";

import "./App.css";
import Modal from "./components/Modal/Modal";
import Backdrop from "./components/Backdrop/Backdrop";
import List from "./components/List/List";

class App extends Component {
  state = {
    modalIsOpen: true,
    showBlock: false,
  }

  toggleModal = (status) => () => {
    this.setState({ modalIsOpen: status })
  }

  render() {
    const { modalIsOpen, showBlock } = this.state
    return (
      <div className="App">
        <h1>React Animations</h1>
        <button onClick={() => this.setState((prev)=>({showBlock: !prev.showBlock}))} > Toogle</button>
        <br/>
        <Modal closed={this.toggleModal} show={modalIsOpen}/>
        <Backdrop show={modalIsOpen} />
        <button className="Button" onClick={this.toggleModal(true)}>Open Modal</button>
        <h3>Animating Lists</h3>
        <List />
      </div>
    );
  }
}

export default App;
