import React from 'react';

import './Backdrop.css';

const backdrop = (props) => {
  const { show } = props
  const cssBack = [
    'Backdrop',
    show ? 'BackdropOpen' : 'BackdropClosed'
  ]
  return(
    <div className={cssBack.join(' ')}/>
  );
}

export default backdrop;
