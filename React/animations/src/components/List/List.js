import React, { Component } from 'react';
import TransitionGroup from "react-transition-group/cjs/TransitionGroup";
import CSSTransition from "react-transition-group/cjs/CSSTransition";

import './List.css';

class List extends Component {
    state = {
        items: [
            {id: 0},
            {id: 2},
            {id: 1}
        ]
    }

    addItemHandler = () => {
        const ar = [ ... this.state.items ]
        ar.push({ id: ar.length + 1 })
        this.setState( {
                items: ar
        });
    }

    removeItemHandler = (selIndex) => {
        this.setState((prevState) => {
            return {
                items: prevState.items.filter((item) => item.id !== selIndex)
            };
        });
    }

    render () {
        const listItems = this.state.items.map( (item) => (
          <CSSTransition key={item.id} classNames="fade" timeout={300}>
            <li
                className="ListItem"
                onClick={() => this.removeItemHandler(item.id)}>{item.id}</li>
          </CSSTransition>
        ) );

        return (
            <div>
                <button className="Button" onClick={this.addItemHandler}>Add Item</button>
                <p>Click Item to Remove.</p>
                <ul className="List">
                    <TransitionGroup component='ul' className='List'>
                        {listItems}
                    </TransitionGroup>
                </ul>
            </div>
        );
    }
}

export default List;
