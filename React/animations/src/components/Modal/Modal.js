import React from 'react';

import './Modal.css';
import CSSTransition from "react-transition-group/cjs/CSSTransition";

const timing = {
  enter: 400,
  exit: 1000
}

const modal = (props) => {
  const { show, closed } = props

  return (
    <CSSTransition in={show} mountOnEnter unmountOnExit timeout={timing} classNames='fade-slide'>
      <div className="Modal">
        <h1>A Modal</h1>
        <button className="Button" onClick={closed(false)}>Dismiss</button>
      </div>
    </CSSTransition>
  )
}

export default modal;

// {/*<Transition in={show} mountOnEnter unmountOnExit timeout={timing}>*/}
// {/*  { state =>  {*/}
// {/*    const cssBack = [*/}
// {/*      'Modal',*/}
// {/*      state === 'entering'*/}
// {/*        ? 'ModalOpen'*/}
// {/*        :  state === 'exiting' ? 'ModalClosed' : null*/}
// {/*    ]*/}
// {/*    return(*/}
// {/*      <div className={cssBack.join(' ')}>*/}
// {/*        <h1>A Modal</h1>*/}
// {/*        <button className="Button" onClick={closed(false)}>Dismiss</button>*/}
// {/*      </div>*/}
// {/*    )*/}
// {/*  }}*/}
// {/*</Transition>*/}
