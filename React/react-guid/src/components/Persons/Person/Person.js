import React, { Component } from 'react';
import classes from './Person.css';
import WithClass from '../../../hoc/WithClass';
import Aux from '../../../hoc/Aux_';
import {AuthContext}  from '../../../containers/App';

class Person extends Component {
    constructor( props ) {
        super( props );
        console.log( '[Person.js] Inside Constructor', props );
        this.inputElement = React.createRef();
    }

    componentWillMount () {
        console.log( '[Person.js] Inside componentWillMount()' );
    }

    componentDidMount () {
        console.log( 'this.inputElement'  + this.inputElement.current.value);

    }
    focus(){
      if(this.props.number === 2)
        this.inputElement.current.focus();
    }
    render () {
        console.log( '[Person.js] Inside render()' );
        return (
            <Aux>
              <AuthContext.Consumer>
                {auth => auth ? <p>true</p>  : <p>false</p>}
              </AuthContext.Consumer>
              <p onClick={this.props.click}>I'm {this.props.name} and I am {this.props.age} years old!</p>
                <p>{this.props.children}</p>
                <input ref={this.inputElement} type="text" onChange={this.props.changed} value={this.props.name} />
            </Aux>
        )
        // return [
        //     <p key="1" onClick={this.props.click}>I'm {this.props.name} and I am {this.props.age} years old!</p>,
        //     <p key="2">{this.props.children}</p>,
        //     <input key="3" type="text" onChange={this.props.changed} value={this.props.name} />
        // ]
    }
}

export default WithClass (Person, classes.Person);