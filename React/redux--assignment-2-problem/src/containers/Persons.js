import React, { Component } from 'react';
import { connect } from 'react-redux'
import Person from '../components/Person/Person';
import AddPerson from '../components/AddPerson/AddPerson';
import * as actionTypes from '../actions/actions'

class Persons extends Component {
    render () {
      console.log('this.props.personss ' ,this.props.persons)
        return (
            <div>
                <AddPerson personAdded={this.props.addPerson} />
                {this.props.persons.map(person => (
                    <Person
                        key={person.id}
                        name={person.name}
                        id={person.id}
                        age={person.age}
                        clicked={() => this.props.deletePerson(person.id)}/>
                ))}
            </div>
        );
    }
}

const mapStateToProps = state =>{
    return{
      persons: state.persons
    }
}

const mapDispatchToProps = dispach => {
    return{
        addPerson: (newPerson) => dispach({type:actionTypes.ADD_PERSON, person: newPerson}),
        deletePerson: (idPerson) => dispach({type:actionTypes.DELETE_PERSON, personElId: idPerson}),
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Persons);