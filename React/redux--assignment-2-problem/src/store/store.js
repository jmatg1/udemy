import * as actionTypes from '../actions/actions';

const initialState = {
  persons: []
};

const reducer = ( state = initialState, action ) => {
  switch ( action.type ) {


    case actionTypes.ADD_PERSON:
      const newPerson = {
        id: Math.floor( Math.random() * 1000 ), // not really unique but good enough here!
        name: 'Max',
        age: Math.floor( Math.random() * 40 )
      };

      return {
        ...state,
        persons: state.persons.concat(newPerson)
      }


    case actionTypes.DELETE_PERSON:
      // const id = 2;
      // const newArray = [...state.results];
      // newArray.splice(id, 1)
      const updatedArray = state.persons.filter(person => person.id !== action.personElId);
      return {
        ...state,
        persons: updatedArray
      }
  }
  return state
};

export default reducer;