import axios_ from "../../axios-orders";
import * as actions from "../actions";
import { put } from 'redux-saga/effects'

export function* initIngredientsSaga(action) {
  try {

    const response = yield axios_.get('/ingredients.json')
    yield put (actions.setIngredients(response.data))

  } catch (er) {

    yield put (actions.fetchIngredientsFailed())
  }
}
