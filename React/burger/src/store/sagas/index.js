import {all} from 'redux-saga/effects'
import * as wathers from './wathers'

export default function* rootSaga() {
  yield all([
    wathers.watchAuth(),
    wathers.watchBurgerBuilder()
  ])
}
