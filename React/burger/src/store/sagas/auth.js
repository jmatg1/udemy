import {delay} from 'redux-saga/effects'
import {put} from 'redux-saga/effects'
import * as actions from '../actions/index'
import axios from "axios";
import {authSuccess} from "../actions/index";
import {checkAuthTimeout} from "../actions/index";
import {authFail} from "../actions/index";
import {logout} from "../actions/index";

export function* logoutSaga(action) {
  yield localStorage.removeItem("token");
  yield localStorage.removeItem("expirationDate");
  yield localStorage.removeItem("userId");
  yield put(actions.logoutSucceed())
}

export function* checkAuthTimeoutSaga(action) {
  yield delay(action.expirationTime)
  yield put(actions.logout())
}

export function* authUserSaga(action) {
  yield put(actions.authStart());

  const authData = {
    email: action.email,
    password: action.password,
    returnSecureToken: true
  };
  let url = 'https://www.googleapis.com/identitytoolkit/v3/relyingparty/signupNewUser?key=AIzaSyBUn8sVpLhg67w0P_t5u5-t-M7nVXYpUjg';
  if (!action.isSignup) {
    url = 'https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword?key=AIzaSyBUn8sVpLhg67w0P_t5u5-t-M7nVXYpUjg';
  }
  try {
    const response = yield axios.post(url, authData)

    const expirationDate = new Date(new Date().getTime() + response.data.expiresIn * 1000)

    yield localStorage.setItem("token", response.data.idToken);
    yield localStorage.setItem("expirationDate", expirationDate);
    yield localStorage.setItem("userId", response.data.localId);

    yield put(actions.authSuccess(response.data.idToken, response.data.localId));
    yield put(checkAuthTimeout(response.data.expiresIn * 1000))

  } catch (err) {
    yield put(actions.authFail(err.response.data.error));
  }
}

export function* authCheckStateSaga(action) {
  const token = localStorage.getItem("token")
  if (token) {
    const expirationDate = new Date(localStorage.getItem("expirationDate"))
    if (expirationDate >= new Date()) {
      const userId = localStorage.getItem("userId")
      yield put(actions.authSuccess(token, userId))
      yield put(actions.checkAuthTimeout(expirationDate.getTime() - new Date().getTime()))
    }
  } else {
    yield put(actions.logout())
  }
}
