import React,{Component} from "react";
import Burger from '../../components/Burger/Burger';
import BuildControls from '../../components/Burger/BuildControls/BuildControls';
import Modal from '../../components/UI/Modal/Modal';
import OrderSummary from '../../components/Burger/OrderSummary/OrderSummary';
import axios_ from '../../axios-orders'
import Spinner from '../../components/UI/Spinner/Spinner';
import  withErrorHandler from '../../hoc/withErrorHandler/withErrorHandler';
import { connect } from 'react-redux'
import * as actions from '../../store/actions/index'

export class BurgerBuilder extends Component{
  state ={
    purchasable: false,
    purchasing: false,
    loading: false
  }


  componentDidMount() {
    this.props.getIngredients();
  }


  updatePurchaseState(ingredients){
    const sum = Object.keys(ingredients)
      .map(igKey=>{
        return ingredients[igKey]
      }).reduce((sum,el) => {
        return sum+el;
      },0);
    return sum > 0
  }
  purchaseHandler = () =>{
    if(this.props.isAuthenticated){
      this.setState({purchasing:true});
    } else {
      this.props.setAuthRedirectPath('/checkout');
      this.props.history.push('/auth')
    }

  }
  purchaseCancelHandler = () =>{
    this.setState({purchasing:false})
  }
  purchaseContinueHandler = () =>{

    this.props.initPurchase()
    this.props.history.push({pathname: '/checkout' });



  }

  render(){

    const disabledInfo = {
      ...this.props.ingredients
    };
    for (let key in disabledInfo){
      disabledInfo[key] = disabledInfo[key] <= 0
    }

    let OrderSummary_ = null

    if(this.state.loading){
      OrderSummary_ = <Spinner/>;
    }
    let burger = this.props.error ?<p>Ingredients can't be loaded!</p> :  <Spinner/>;
    if(this.props.ingredients){
       burger = (
        <>
          <Burger ingredients={this.props.ingredients} />
          <BuildControls
          purchasable={this.updatePurchaseState(this.props.ingredients)}
          price={this.props.totalPrice}
          disabledButton={disabledInfo}
          ingredientRemoved={this.props.removeIngredient}
          ingredientAdded={this.props.addIngredient}
          isAuth={this.props.isAuthenticated}
          order={this.purchaseHandler}/>
        </>
      );
      OrderSummary_  = (
      <OrderSummary
        continue={this.purchaseContinueHandler}
        cancel={this.purchaseCancelHandler}
        ingredients={this.props.ingredients}
        totalPrice={this.props.totalPrice}/>
      );
    }
    return (
      <>
      <Modal show={this.state.purchasing} modalClosed={this.purchaseCancelHandler}>
        {OrderSummary_}
      </Modal>
      { burger}
      </>
    );


  }
}
const mapStateToProps = state =>{
  return {
    ingredients: state.burgerBuilder.ingredients,
    totalPrice: state.burgerBuilder.totalPrice,
    error: state.burgerBuilder.error,
    purchased: state.order.purchased,
    isAuthenticated: state.auth.token !== null
  }

}

const mapDispatchToProps = dispatch => {
  return{
    addIngredient: (ingredientName) => dispatch(actions.addIngredient(ingredientName)),
    removeIngredient: (ingredientName) => dispatch(actions.removeIngredient(ingredientName)),
    getIngredients: () => dispatch(actions.initIngredients()),
    initPurchase: () => dispatch(actions.purchaseInit()),
    setAuthRedirectPath: (path) => dispatch(actions.setAuthRedirectPath(path))
  }

}

export default connect(mapStateToProps,mapDispatchToProps)(withErrorHandler(BurgerBuilder, axios_));