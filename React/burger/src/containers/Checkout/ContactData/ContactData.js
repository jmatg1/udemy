import React, {Component} from 'react';
import classes from './ContactData.css';
import Button from '../../../components/UI/Button/Button';
import axios_ from '../../../axios-orders';
import Spinner from '../../../components/UI/Spinner/Spinner';
import Input from '../../../components/UI/Input/Input'
import { connect } from 'react-redux'
import withErrorHandler from "../../../hoc/withErrorHandler/withErrorHandler";
import * as  actions from '../../../store/actions/index'
import {updateObject , checkValidity} from "../../../shared/utility";

class ContactData extends Component {
  state = {
    orderForm:{
      name: {
        elementType: 'input',
        elementConfig: {
          type: 'text',
          placeholder: 'Your Name'
        },
        value: '',
        validation:{
          required:true
        },
        valid:false,
        touched: false
        
      },
      street:{
        elementType: 'input',
        elementConfig: {
          type: 'text',
          placeholder: 'Street'
        },
        value: '',
        validation:{
          required:true
        },
        valid:false,
        touched: false
      },
      zipCode:{
        elementType: 'input',
        elementConfig: {
          type: 'text',
          placeholder: 'ZIP Code'
        },
        value: '',
        validation:{
          required:true,
          minLength: 3,
          maxLength: 8
        },
        valid:false,
        touched: false
      },
      country:{
        elementType: 'input',
        elementConfig: {
          type: 'text',
          placeholder: 'Country'
        },
        value: '',
        validation:{
          required:true
        },
        valid:false,
        touched: false
      },
      email:{
        elementType: 'input',
        elementConfig: {
          type: 'email',
          placeholder: 'Your E-Mail'
        },
        value: '',
        validation:{
          required:true,
          isEmail: true
        },
        valid:false,
        touched: false
      },
      deliveryMethod:{
        elementType: 'select',
        elementConfig: {
         options:[
           {value:'fastest', displayValue: 'Fastest'},
           {value:'slowly', displayValue: 'Slowly'},
         ]
        },
        value: 'fastest',
        validation:{
          required:false
        },
        valid: true
      }
    },
    formIsValid: false
  };
  orderHandler = (event)=>{
    event.preventDefault();
    const formData = {}
    for (let formElementIdentifier in this.state.orderForm){
      formData[formElementIdentifier] = this.state.orderForm[formElementIdentifier].value
    }
    const order = {
      ingredients: this.props.ingredients,
      price: this.props.totalPrice,
      orderData: formData,
      userId: this.props.userId
    };
    this.props.orderBurger(order, this.props.token);
  }
  inputChangedHandler = (event, id) =>{
    const updatedFormElement = updateObject(this.state.orderForm[id], {
      value: event.target.value,
      valid: checkValidity(event.target.value, this.state.orderForm[id].validation),
      touched: true
    })
    const updateOrderForm = updateObject(this.state.orderForm,{
      [id]: updatedFormElement
    })

    let formIsValid = true;
    for (let inputIdentifier in updateOrderForm){
      formIsValid = updateOrderForm[inputIdentifier].valid && formIsValid
    }


    this.setState({orderForm:updateOrderForm, formIsValid: formIsValid })
  }
  render () {
    const formElementsArray = [];
    for (let key in this.state.orderForm){
      formElementsArray.push({
        id: key,
        config: this.state.orderForm[key]
      })
    }
    let form = (
      <form onSubmit={this.orderHandler}>
        {formElementsArray.map(formElements =>(
          <Input key={formElements.id}
                 elementType={formElements.config.elementType}
                 value={formElements.config.value}
                 elementConfig={formElements.config.elementConfig}
                 invalid={!formElements.config.valid}
                 shouldValidate={formElements.config.validation}
                 touched={formElements.config.touched}
                 changed={(event) => this.inputChangedHandler(event,formElements.id )}/>
        ))}
        <Button disabled={!this.state.formIsValid} btnType="Success" clicked={this.orderHandler}>ORDER</Button>
      </form>
    );
    if ( this.props.loading ) {
      form = <Spinner />;
    }
    return (
      <div className={classes.ContactData}>
        <h4>Enter your Contact Data</h4>
        {form}
      </div>
    );
  }
}

const mapStateToProps = state =>{
  return {
    ingredients: state.burgerBuilder.ingredients,
    totalPrice: state.burgerBuilder.totalPrice,
    loading: state.order.loading,
    token: state.auth.token,
    userId: state.auth.userId
  }
}

const mapDispatchToProps = dispatch => {
  return{
    orderBurger: (orderData, token) => dispatch(actions.purchaseBurger(orderData, token))
  }
}
export default connect(mapStateToProps,mapDispatchToProps)(withErrorHandler(ContactData, axios_));
