import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://burger-be1ce.firebaseio.com/',

});
export default instance;