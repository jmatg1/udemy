import React from 'react';
import classes from './SideDrawer.css';
import Logo from '../Logo/Logo';
import NavigationItems from '../Navigation/NavigationItems/NavigationItems';
import Backdrop from '../UI/Backdrop/Backdrop';
const sideDrawer = (props) => {
  const classStatus = props.show ? classes.Open : classes.Close
  return(
    <>
    <div className={[classes.SideDrawer, classStatus].join(' ')} onClick={props.clicked}>
      <div className={classes.Logo}>
        <Logo/>
      </div>
      <nav className={classes.Naviganion}>
        <NavigationItems isAuthenticated={props.isAuth}/>
      </nav>
    </div>
    <Backdrop cliked={props.clicked} show={props.show}/>
  </>
    );
};

export default sideDrawer