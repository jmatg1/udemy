import Backdrop from '../Backdrop/Backdrop';
import classes from './Modal.css';
import React, {Component} from 'react';


class Modal extends Component {

  shouldComponentUpdate(nextProps, nextState) {
    return nextProps.show !== this.props.show || nextProps.children !== this.children
  }

  render() {
    return (
      <>
      <Backdrop show={this.props.show} cliked={this.props.modalClosed}/>
      <div
        style={{
          transform: this.props.show ? 'translateY(0)' : 'translateY(-100vh)',
          opacity: this.props.show ? '1' : '0',
          display: this.props.show ? '' : 'none'
        }}
        className={classes.Modal}>

        {this.props.children}

      </div>
      </>
    );
  }
}



export default Modal;
