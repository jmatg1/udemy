import React, {Component} from "react";
import { connect } from 'react-redux'
import Aux from '../Aux_/Aux_'
import classes from './Layout.css'
import Toolbar from '../../components/Navigation/Toolbar/Toolbar'
import SideDrawer from '../../components/SideDrawer/SideDrawer'
class Layout extends Component {
  state = {
  toggleMobileMenu: false
  }
  toggleMobileMenuHandler = (status) =>{
    this.setState({toggleMobileMenu: status});
  }
  render() {
    return (
      <Aux>
        <Toolbar
          isAuth={this.props.isAuthenticated}
          clicked={this.toggleMobileMenuHandler}/>
          <SideDrawer
            isAuth={this.props.isAuthenticated}
            clicked={this.toggleMobileMenuHandler.bind(this,false)}
            show={this.state.toggleMobileMenu}/>


        <main className={classes.Content}>
          {this.props.children}
        </main>
      </Aux>
    )
  }

};

const mapStateToProps = state => {
  return {
    isAuthenticated: state.auth.token !== null
  }
}

export default connect(mapStateToProps)(Layout)