import axios from 'axios'

const instance = axios.create({
  baseURL:'http://localhost:3001/posts',
});

instance.headers.common['Authorization'] = 'TOKEN';
instance.headers.post['Content-Type'] = 'aplication/json';