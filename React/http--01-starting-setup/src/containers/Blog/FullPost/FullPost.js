import React, { Component } from 'react';
import axios from 'axios';
import './FullPost.css';

class FullPost extends Component {
  state = {
    fullPost: null,
    error: false
  }


  componentDidMount() {
    this.loadData ()
  }

  componentDidUpdate(prevProps, prevState) {
    console.log(prevProps);
    if(this.props.match.params.id !== prevProps.match.params.id) {
      this.loadData ()
    }
  }
  loadData () {
    if(this.props.id || this.props.match.params.id)
      if( !this.state.fullPost || (this.state.fullPost && this.state.fullPost.id !== this.props.id)) {
        axios.get('http://localhost:3001/posts/' + this.props.match.params.id)
          .then(response => {
            this.setState({fullPost: response.data});
          })
      }
  }
  deletePostHandler = () =>{
    axios.delete('http://localhost:3001/posts/'+this.props.match.params.id)
      .then(response =>{
       this.post = <p>Delete Completed!</p>
        this.setState({fullPost: null});
      })
      .catch(error=>{
        this.setState({error: true});
      })
  }
  render () {
      this.post = <p style={{textAlign: 'center'}}>Please select a Post!</p>;
      if(this.state.fullPost)
        this.post = <p style={{textAlign: 'center'}}>Loading!</p>;
      if(this.state.fullPost)
        this.post = (
          <div className="FullPost">
            <h1>{this.state.fullPost.title}</h1>
            <p>{this.state.fullPost.content}</p>
            <div className="Edit">
              <button onClick={this.deletePostHandler} className="Delete">Delete</button>
            </div>
          </div> );
      if(this.state.error)
        this.post = <p>Post was delete</p>
        return this.post;
    }
}

export default FullPost;