import React, {Component} from 'react';
import axios from 'axios';
import { Link, Route } from 'react-router-dom';

import Post from '../../../components/Post/Post'
import FullPost from '../FullPost/FullPost'
import Spinner from '../../../components/Spinner/Spinner'
import  './Posts.css'

class Posts extends Component {
  state= {
    posts: []

  }
  componentDidMount() {
    axios.get('http://localhost:3001/posts')
      .then(response=>{
        this.setState({posts: response.data})
      });
  }
  showPostHandler = (id) => {
    this.props.history.push({pathname: '/posts/' + id})
  }

  render() {
    const posts = this.state.posts.map(post => {
      return (
        // {/*<Link key={post.id} to={'/' + post.id}>*/}
        <Post
          key={post.id}
        id={post.id}
        clicked={()=>this.showPostHandler(post.id)}
        title={post.title}/>
       // </Link>
      );
    });
    console.log(posts)
    return (
      <div>
      <section className="Posts">
        {posts.length === 0 ? <Spinner/> :posts }
      </section>
      <Route path={this.props.match.url + "/:id" } exact component={FullPost}/>
      </div>
      );
  }
}
export default Posts;
