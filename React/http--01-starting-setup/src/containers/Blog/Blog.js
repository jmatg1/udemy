import React, { Component } from 'react';
import { Route, NavLink, Switch, Redirect } from "react-router-dom";

import Post from '../../containers/Blog/Posts/Posts';
// import FullPost from './FullPost/FullPost'
// import NewPost from '../../containers/Blog/NewPost/NewPost';
import asyncComponent from '../../hoc/asyncComponent'

import './Blog.css';


const AsyncNewPost = asyncComponent(()=>{
  return import ('../../containers/Blog/NewPost/NewPost')
})

class Blog extends Component {
  state = {
    auth: true
  }


  render () {

        return (
            <div className="Blog">
              <header>
                <nav>
                  <ul>
                    <li><NavLink to="/posts/" exact >Home</NavLink></li>
                    <li><NavLink to={{
                      pathname: '/new-post',
                      hash: '#submit',
                      search: '?quick-submit=true'
                      }}>New Post</NavLink></li>
                  </ul>
                </nav>
              </header>
              <Switch>
                <Route path="/new-post" exact component={AsyncNewPost}/>
                <Route path="/posts/"  component={Post}/>
                <Route render={() => <h1> Not found 404 </h1>}></Route>
                <Redirect from="/" to="/posts"/>
              </Switch>
                {/*<section>*/}
                    {/*<FullPost id={this.state.selectedPostId} />*/}
                {/*</section>*/}
                {/*<section>*/}
                    {/*<NewPost />*/}
                {/*</section>*/}
            </div>
        );
    }
}

export default Blog;