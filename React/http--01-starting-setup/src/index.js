import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import axios from 'axios';

// axios.defaults.baseURL = 'http://localhost:3001/posts';
// axios.defaults.headers.common['Authorization'] = 'TOKEN';
// axios.defaults.headers.post['Content-Type'] = 'aplication/json';
axios.interceptors.request.use(request =>{
  // console.log(request);
  return request
})
ReactDOM.render( <App />, document.getElementById( 'root' ) );
registerServiceWorker();
