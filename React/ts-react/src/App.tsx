import React from 'react';
import logo from './logo.svg';
import './App.css';

/**
 * Такой хороший и мой
 */
interface iUser {
    name: string,
    id: number
}

const App: React.FC = () => {
  const roman: iUser = {
    name: 'Roman',
    id: 26
  }
  roman.id = 26

  return (
  <div className="App">
      <header className="App-header">
              <img src={logo} className="App-logo" alt="logo"/>
              <p>
                  Edit <code>src/App.tsx</code> and save to reload.
              </p>
              <a
                  className="App-link"
                  href="https://reactjs.org"
                  target="_blank"
                  rel="noopener noreferrer"
              >
                  Work!
              </a>
          </header>
    </div>
    );
}

export default App;
