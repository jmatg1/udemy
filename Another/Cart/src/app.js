import Cart from "./modules/Cart";
import CartItem from "./modules/CartItem";
import { currencies } from "./modules/currencies";

const items = [
  { quantity: 2, price: '$3.95' },
  { quantity: 7, price: '$5.80' },
  { quantity: 1, price: '$12.83' },
]
console.log('items', items)

const cart = new Cart({ name: 'Test', currentCurrency: '$' })
cart.addItems(items)
console.log(`Cart:`, cart)

cart.cartItemRemove(0)
console.log(`Remove:`, cart.items)

console.log(`TotalPrice: ${cart.totalPrice}`)

console.log('Change currency')

cart.changeCurrency('EUR')

console.log(`TotalPrice: ${cart.totalPrice}`)

cart.changeCurrency('USD')

console.log(`TotalPrice: ${cart.totalPrice}`)

console.log(`Items:`, cart.items)
