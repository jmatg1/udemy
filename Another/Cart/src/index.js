const currencies = {
  USD: {
    code: 'USD',
    name: 'Dollar',
    sign: '$',
    EUR: 0.9,
  },
  EUR: {
    code: 'EUR',
    name: 'Euro',
    sign: '\u20AC',
    USD: 1.11,
  }
}

class Cart{
  constructor (settings) {
    this.cartItems = [];
    this.currentCurrency = settings.currentCurrency
  }

  /**
   * Get the total amount
   */
  get totalPrice () {
    const total = this.cartItems.reduce((acc,item) =>(acc + item.totalPrice), 0).toFixed(2)
    return `${this.currentCurrency}${total}`
  }

  /**
   * Get all items
   */
  get items () {
    return this.cartItems.map(item => item.item).slice()
  }

  /**
   * Add card to cart
   * @param cartItem {object}
   */
  cartItemAdd (cartItem) {
    this.cartItems.push(cartItem)
  }

  /**
   * Add an array of items to the cart
   * @param itemList {object}
   * @param itemList.quantity {number}
   * @param itemList.price {string}
   */
  addItems (itemList) {
    itemList.forEach((el, i) => {
      this.cartItemAdd(new CartItem(i, el.quantity, el.price))
    })
  }
  /**
   * Remove card from cart
   * @param id {number}
   */
  cartItemRemove (id) {
    this.cartItems = this.cartItems.filter(item => item.id !== id)
  }

  /**
   * Change currency. Changes the price of each product.
   * @param code {string}
   */
  changeCurrency (code) {
    this.cartItems.map(item => item.changeCurrency(code))
    this.currentCurrency = currencies[code].sign
  }

}

class CartItem {
  constructor(id, quantity, price) {
    this.id = id
    this.quantity = quantity
    this.price = Number(price.slice(1, price.length))
    this.currency = {
      sign: price.slice(0, 1),
      code: ''
    }

    for (let key in currencies) {
      if(currencies[key].sign === this.currency.sign) {
        this.currency.code = currencies[key].code
      }
    }
  }

  /**
   * Convert to another currency. Change price
   * @param code
   */
  changeCurrency (code){
    const exRate = currencies[code][this.currency.code] // Exchange Rate
    // console.log(`${this.currency.code} -> ${code} = ${x} * ${this.price} = ${this.price * x} `)
    this.price = Number((this.price * exRate))
    this.currency.sign = currencies[code].sign
    this.currency.code = code
    console.log();
  }

  /**
   * Get the total price of the item
   * @returns {number}
   */
  get totalPrice () {
    return this.quantity * this.price
  }

  get item () {
    return {
      quantity: this.quantity,
      price: `${this.currency.sign}${this.price.toFixed(2)}`,
    }
  }
}

const items = [
  { quantity: 2, price: '$3.95' },
  { quantity: 7, price: '$5.80' },
  { quantity: 1, price: '$12.83' },
]

console.log('items', items)

const cart = new Cart({ name: 'Test', currentCurrency: '$' })
cart.addItems(items)
console.log(`Cart:`, cart)

cart.cartItemRemove(0)
console.log(`Remove:`, cart.items)

console.log(`TotalPrice: ${cart.totalPrice}`)

console.log('Change currency')

cart.changeCurrency('EUR')

console.log(`TotalPrice: ${cart.totalPrice}`)

cart.changeCurrency('USD')

console.log(`TotalPrice: ${cart.totalPrice}`)

console.log(`Items:`, cart.items)
