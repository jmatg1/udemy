import {currencies} from "./currencies";

export default class CartItem {
  constructor(id, quantity, price) {
    this.id = id
    this.quantity = quantity
    this.price = Number(price.slice(1, price.length))
    this.currency = {
      sign: price.slice(0, 1),
      code: ''
    }

    for (let key in currencies) {
      if(currencies[key].sign === this.currency.sign) {
        this.currency.code = currencies[key].code
      }
    }
  }

  /**
   * Convert to another currency. Change price
   * @param code
   */
  changeCurrency (code){
    const exRate = currencies[code][this.currency.code] // Exchange Rate
    // console.log(`${this.currency.code} -> ${code} = ${x} * ${this.price} = ${this.price * x} `)
    this.price = Number((this.price * exRate))
    this.currency.sign = currencies[code].sign
    this.currency.code = code
    console.log();
  }

  /**
   * Get the total price of the item
   * @returns {number}
   */
  get totalPrice () {
    return this.quantity * this.price
  }

  get item () {
    return {
      quantity: this.quantity,
      price: `${this.currency.sign}${this.price.toFixed(2)}`,
    }
  }
}
