export const currencies = {
  USD: {
    code: 'USD',
    name: 'Dollar',
    sign: '$',
    EUR: 0.9,
  },
  EUR: {
    code: 'EUR',
    name: 'Euro',
    sign: '\u20AC',
    USD: 1.11,
  }
}
