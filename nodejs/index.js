import * as fs from 'fs'
import {fileURLToPath} from "url";
import {dirname} from "path";
const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

console.log('Start');

setTimeout(() => {
    console.log(performance.now());
}, 0)

setImmediate(() => {
    console.log('setImmediate');
})

fs.readFile(__filename, () => {
    console.log('Readed');
})

setTimeout(() => {
   for (let i =0; i<999999999; i++){

   }
    console.log('Done');
}, 0)

Promise.resolve().then(()=>{
    console.log('Promise');
    Promise.resolve().then(()=>{
        console.log('Promise2');
    })

})
process.nextTick(()=>{
    console.log('tick');
})

console.log('End');