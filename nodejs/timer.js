

const f = (a,b) => {
    console.log(a + b);
}

// const start = performance.now();
// console.log('Start');
// setImmediate(() => {
//     console.log('IMEDIATE');
// })
// console.log('End');

const timer = setTimeout(() => {
    console.log('BOOM');
}, 5000)
timer.unref()
setImmediate(() => {
    timer.ref()
})