import { fileURLToPath } from 'url';
import { dirname } from 'path';
import { Module } from 'module'
const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

import { EventEmitter } from 'events'

const myEmitter = new EventEmitter();


const logDB = () => {
    console.log('DB CONNECTED');
}

myEmitter.addListener('connected', logDB);

myEmitter.emit('connected');



// myEmitter.off('connected', logDB)
myEmitter.on('msg', (data) => {
    console.log(data);
})
myEmitter.emit('msg', '            qqq');

myEmitter.once('once', () => {
    console.log('once');
})

myEmitter.emit('once')
myEmitter.emit('once')
myEmitter.setMaxListeners(1);
myEmitter.prependListener('asd', () => {})
console.log(myEmitter.listenerCount('msg'));
console.log(myEmitter.listeners('connected'));

myEmitter.on('error', (err) => {
    console.log('Error:' + err);
})

myEmitter.emit('error', new Error('BOOM'))


const target = new EventTarget();

const logTarget = () => {
    console.log('TArget');
}

target.addEventListener('connect', logTarget)

target.dispatchEvent(new Event('connect'))